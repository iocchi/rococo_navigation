import math, random
import argparse
import os
import random
import numpy as np
from matplotlib.image import imread
from matplotlib import pyplot as plt
from argparse import RawTextHelpFormatter

import rospy
import actionlib
from threading import Thread

from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist, Quaternion
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
import tf

ACTION_move_base = '/diago_0/move_base'
TOPIC_cmd_vel = '/diago_0/cmd_vel'
TOPIC_desired_cmd_vel = '/diago_0/desired_cmd_vel'
TOPIC_odom = '/diago_0/odom'
TOPIC_laser = '/diago_0/scan'
FRAME_map = '/diago_0/map'
FRAME_base = '/diago_0/base_frame'

PARAM_obstNear = '/diago_0/gradient_based_navigation/obstacleNearnessEnabled'

map_image_path = '/home/ubuntu/src/stage_environments/maps/Labirinto.png'


odom_robot_pose = [0, 0, 0]
map_robot_pose = [0, 0, 0]
robot_vel = [0, 0]
robot_desired_vel = [0, 0]
target_pose = [0, 0, 0]
odomcount = 0 
odomframe = ''
log_pgv = []
log_recovery = []

# PARAMETERS METRICS COMPUTATION
global amod_container
amod_container = []
global dd_counter
dd_counter = 0
global starting_time
starting_time = 0
global doLog



listener = None

def load_map_image(map_image_path):
    img = imread(map_image_path)
    return img

def draw_path_image(img):
    global log_pgv
    log_pgv_arr = np.array(log_pgv)
    log_p = log_pgv_arr[:,0:2] # take only x and y coordinates
    
    for position in log_p:
        img_position = np.abs([0.0, img.shape[0]] - np.round(position / 0.075)).astype(dtype=int)
        img[img_position[1] - 4: img_position[1] - 1, img_position[0] - 4: img_position[0] - 1,:] = [1.0,0.0,0.0,1.0]
    
    return img



def laser_cb(laser):
    global min_r
    min_r = laser.ranges
    min_r = min(min_r)
    amod_container.append(min_r)

def compute_amod(amod_container):
    amod = sum(amod_container) / len(amod_container)
    return amod 

def compute_tg():
    global starting_time
    t_goal = rospy.Time.now()
    t_goal_secs = t_goal.secs
    print(t_goal_secs)
    return t_goal_secs - starting_time

def compute_dd(map_robot_x, map_robot_y):
    global dd_counter
    curr_cell = current_cell(map_robot_x, map_robot_y)
    if curr_cell in dangerous_squares:
        dd_counter += 1
        print("Robot in dangerous zone. Cell: (%s,%s)" %(curr_cell[0], curr_cell[1]))


def current_cell(map_robot_x, map_robot_y):
    floor_x = math.floor(map_robot_x)
    floor_y = math.floor(map_robot_y)
    curr_cell = [floor_x, floor_y]
    return curr_cell

 
def odom_cb(data):
    global odom_robot_pose, odomcount, odomframe, doLog
    odom_robot_pose[0] = data.pose.pose.position.x
    odom_robot_pose[1] = data.pose.pose.position.y
    o = data.pose.pose.orientation
    q = (o.x, o.y, o.z, o.w)
    euler = tf.transformations.euler_from_quaternion(q)
    odom_robot_pose[2] = euler[2] # yaw
    odomcount += 1
    odomframe = data.header.frame_id
    if doLog:
        log_current_state()

def get_robot_pose():
    global map_robot_pose, dd_counter
    try:
        (trans,rot) = listener.lookupTransform(FRAME_map, FRAME_base, rospy.Time(0))
    except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException) as e:
        print(e)
        return False

    roll, pitch, yaw = tf.transformations.euler_from_quaternion(rot)
    map_robot_pose[0] = trans[0]
    map_robot_pose[1] = trans[1]
    map_robot_pose[2] = yaw

    compute_dd(map_robot_pose[0], map_robot_pose[1])

    return True


def dist_from_goal():
    global map_robot_pose, target_pose
    if (get_robot_pose()):
        return math.sqrt(math.pow(map_robot_pose[0]-target_pose[0],2)+math.pow(map_robot_pose[1]-target_pose[1],2))
    else:
        return 999


def cmdvel_cb(data):
    global robot_vel
    robot_vel[0] = data.linear.x
    robot_vel[1] = data.angular.z

def desired_cmdvel_cb(data):
    global robot_vel
    robot_desired_vel[0] = data.linear.x
    robot_desired_vel[1] = data.angular.z


ac_movebase = None 
move_base_running = False

def exec_movebase(gx, gy, gth_deg):
    global ac_movebase, move_base_running
    if (ac_movebase == None):
        ac_movebase = actionlib.SimpleActionClient(ACTION_move_base,MoveBaseAction)
    ac_movebase.wait_for_server()

    goal = MoveBaseGoal()
    goal.target_pose.header.frame_id = FRAME_map
    goal.target_pose.header.stamp = rospy.Time.now()
    goal.target_pose.pose.position.x = gx
    goal.target_pose.pose.position.y = gy
    yaw = gth_deg/180.0*math.pi
    q = tf.transformations.quaternion_from_euler(0, 0, yaw)
    goal.target_pose.pose.orientation = Quaternion(q[0],q[1],q[2],q[3])

    ac_movebase.send_goal(goal)
    move_base_running = True
    print('Moving to %.2f %.2f %.2f' %(gx,gy,gth_deg))

    ac_movebase.wait_for_result()  # blocking until goal is reached

    print('Move action completed.')
    move_base_running = False


def stop_movebase():
    global ac_movebase, move_base_running
    if (ac_movebase == None):
        ac_movebase = actionlib.SimpleActionClient('move_base',MoveBaseAction)
    try:
        ac_movebase.wait_for_server()
        print('Cancel move_base goal')
        ac_movebase.cancel_all_goals()
    except:
        pass
    move_base_running = False


thr_movebase = None
quit_request = False

def start_movebase(tpose):
    global thr_movebase, target_pose
    target_pose = tpose
    thr_movebase = Thread(target=exec_movebase, args=(target_pose[0], target_pose[1], target_pose[2],))
    thr_movebase.start()


def join_movebase():
    global thr_movebase
    thr_movebase.join()
    thr_movebase = None


def log_current_state():
    global map_robot_pose, target_pose, robot_vel, log_pgv
    get_robot_pose() # set map_robot_pose
    
    #define current log    
    log_curr = [map_robot_pose[0],map_robot_pose[1],map_robot_pose[2],
          target_pose[0],target_pose[1],target_pose[2],
          robot_vel[0],robot_vel[1]]
    
    #update the log list of lists     
    log_pgv.append(log_curr)

def empty_log_buffer():
    global log_pgv, log_recovery
    log_pgv = []
    log_recovery = []

def save_recovery_situation():
    global map_robot_pose, target_pose
    global robot_vel, robot_desired_vel
    global log_recovery
    
    log_recovery_curr = [map_robot_pose[0],map_robot_pose[1],map_robot_pose[2],
          target_pose[0],target_pose[1],target_pose[2]]

    log_recovery.append(log_recovery_curr)

    print('    %.1f,%.1f,%.1f, %.1f,%.1f,%.1f, %.2f,%.2f, %.2f,%.2f' 
        %(map_robot_pose[0],map_robot_pose[1],map_robot_pose[2],
            target_pose[0],target_pose[1],target_pose[2],
            robot_vel[0],robot_vel[1],robot_desired_vel[0],robot_desired_vel[1]))

def do_log_parser(do_log):
    if do_log == "True":
        doLog = True
    elif do_log == "False":
        doLog = False
    else:
        assert("do_log should be either True either False")
    return doLog
        

def do_recovery():
    save_recovery_situation()
    print('  *** recovery: standard nav for 5 seconds ***')
    rospy.set_param(PARAM_obstNear, False)
    rate2 = rospy.Rate(1) # Hz
    for i in range(0,5):
        rate2.sleep()
    rospy.set_param(PARAM_obstNear, True)
    print('  *** recovery: stealth nav ***')


quit_request = False

def doSleep(rate):
    global move_base_running, quit_request      
    try:
        rate.sleep()
    except:
        quit_request = True
        move_base_running = False


def write2file(path):
    global log_pgv,log_recovery
    print("Appending position-goal-velocity log of last goal to specified input file ...")

    with open(path,"a") as f:
        for outs in log_pgv:
            for value in outs:
                f.write("%.2f " % value)
            f.write("\n")

    file_list = os.path.splitext(path)
    recovery_file = file_list[0] + "_recovery" + file_list[1]

    print("Appending recovery log to file \"filename_recovery.extension\" ...")
    with open(recovery_file,"a") as f:
        for outs in log_recovery:
            for value in outs:
                f.write("%.2f " % value)
            f.write("\n")


def reach_starting_pose(starting_pose):
    global quit_request, move_base_running
    
    start_movebase(starting_pose)
    starting_time = rospy.Time.now().secs


    # wait until move_base is actually started
    delay = 0.25 # sec
    rate = rospy.Rate(1/delay) # Hz
    print('wait until move_base is actually started')
    while not move_base_running:
        doSleep(rate)

    # Phase 1: normal nav for 5 seconds
    print('standard navigation for reaching starting pose')
    rospy.set_param(PARAM_obstNear, False)
    rate2 = rospy.Rate(1) # Hz

    while move_base_running and not rospy.is_shutdown() and dist_from_goal()>0.5:
        doSleep(rate)
    
    stop_movebase()
    print("reached starting pose")
    join_movebase()


def standard_nav(target_pose):
    global quit_request, move_base_running, starting_time

    start_movebase(target_pose)
    starting_time = rospy.Time.now().secs

    # wait until move_base is actually started
    delay = 0.25 # sec
    rate = rospy.Rate(1/delay) # Hz
    print('0. wait until move_base is actually started')
    while not move_base_running:
        doSleep(rate)

    # Standard nav for 5 seconds
    print('Executing standard navigation ')
    rospy.set_param(PARAM_obstNear, False)
    while move_base_running and not rospy.is_shutdown() and dist_from_goal()>0.5:
        doSleep(rate)

    stop_movebase()

    join_movebase()


def viewshed_nav(target_pose):
    global quit_request, move_base_running, starting_time

    start_movebase(target_pose)
    starting_time = rospy.Time.now().secs

    # wait until move_base is actually started
    delay = 0.25 # sec
    rate = rospy.Rate(1/delay) # Hz
    print('0. wait until move_base is actually started')
    while not move_base_running:
        doSleep(rate)

    # Standard nav for 5 seconds
    print('Executing stealth navigation ')
    rospy.set_param(PARAM_obstNear, True)
    while move_base_running and not rospy.is_shutdown() and dist_from_goal()>0.5:
        doSleep(rate)
    
    print('Standard navigation (until goal)')
    rospy.set_param(PARAM_obstNear, False)
    
    while move_base_running and not rospy.is_shutdown() and dist_from_goal()>0.5:

        doSleep(rate)

    stop_movebase()

    join_movebase()


   
# Reach target pose through adaptive navigation
def adaptive_nav(target_pose):
    global quit_request, move_base_running, starting_time

    start_movebase(target_pose)
    starting_time = rospy.Time.now().secs


    # wait until move_base is actually started
    delay = 0.25 # sec
    rate = rospy.Rate(1/delay) # Hz
    print('0. wait until move_base is actually started')
    while not move_base_running:
        doSleep(rate)

    # Phase 1: normal nav for 5 seconds
    print('1. standard navigation (5 seconds)')
    rospy.set_param(PARAM_obstNear, False)
    rate2 = rospy.Rate(1) # Hz
    #no recovery situation
    for i in range(0,5):
        doSleep(rate2)

    # Phase 2: adaptive nav until 5 meters from the goal
    print('2. adaptive navigation (until 5 meters from goal)')
    rospy.set_param(PARAM_obstNear, True)
    count_slow = 0
    while move_base_running and not rospy.is_shutdown() and dist_from_goal()>5.0:
        

        # check
        if robot_vel[0]<0.1:
            count_slow += 1
            #if count_slow>7:
            #    print('  --DEBUG-- tv = %.2f  (%d)' %(robot_vel[0],count_slow))
            if count_slow>10:
                do_recovery()
        else:
            count_slow = 0
        doSleep(rate)

    # Phase 3: normal nav until the goal
    print('3. standard navigation (until goal)')
    rospy.set_param(PARAM_obstNear, False)
    
    while move_base_running and not rospy.is_shutdown() and dist_from_goal()>0.5:

        doSleep(rate)

    stop_movebase()

    join_movebase()


def execute_navigation_type(target_pose, navigation_type):
    
    if navigation_type == "standard":
      standard_nav(target_pose)
    elif navigation_type == "viewshed":
      viewshed_nav(target_pose)
    elif navigation_type == "adaptive":
      adaptive_nav(target_pose)
    else:
      raise TypeError("The navigation type {} is not a valid navigation type. You should choose between standard, viewshed or adaptive".format(navigation_type)) 


def build_dangerous_squares(dangerous_centers, radius):
    global dangerous_squares

    dangerous_squares = []
    x_idx = 0
    y_idx = 1

    for idx,center in enumerate(dangerous_centers):

        # write the squares changing x-coordinates        
        dangerous_x = center[x_idx]
        dangerous_y = center[y_idx]
        
        dangerous_xs = range(dangerous_x - radius, dangerous_x + radius + 1)
        dangerous_ys = range(dangerous_y - radius, dangerous_y + radius + 1)
        
        for dan_x in dangerous_xs:
            for dan_y in dangerous_ys:
                dangerous_squares.append([dan_x, dan_y])

       
          
# main
if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='obstacle nearness navigation',formatter_class=RawTextHelpFormatter)
    parser.add_argument('do_log', type=str, help='Type True for log recording, False otherwise.')
    parser.add_argument('path', type=str, help='Path to the file where the robot position-goal-velocity log is written. For the recovery log a file \"filename_recovery.extension\" is created. Write any character if do_log is set to False.')
    parser.add_argument('seed', type=int, help='Enter a seed for the reproducibility of the experiment.')
    parser.add_argument('n_goals', type=int, help='Enter the number of goals that must be reached in for the multi-goal case. Put any number for the other cases.')
    parser.add_argument('navigation_type', type=str, help='Type navigation type that the robot is executing to reach the goal. Choose between: standard, viewshed, adaptive')
    requiredNamed = parser.add_argument_group('arguments required for task selection')
    requiredNamed.add_argument('-g','--goal_list', type=float, nargs='+', action='append', help='First case: \n - Write a \"list of lists\" of values on terminal as: -g StartX StartY StartTH -g GoalX GoalY GoalTH to drive the robot from an initial pose Start to a final pose Goal. If the robot is not at Start, it will navigate (normally) to Start and then it will perform the task. \n Second case: \n - Write a \"list of lists\" of values on terminal as: -g nan -g GoalX GoalY GoalTH to drive the robot from an initial random pose nan selected from a predifined set to a final pose Goal. If the robot is not at Start, it will navigate (normally) to nan and then it will perform the task. \n Third case: \n- Write a \"list of lists\" of values on terminal as: -g nan -g nan to continuously drive the robot from an initial random pose nan selected from a predifined set to a final random pose nan (again predefined). The robot in this case will directly start from its current pose.')



    #parse the arguments
    args = parser.parse_args()
    doLog = do_log_parser(args.do_log)
    path = args.path
    s = args.seed
    goal_list = args.goal_list
    num_goals = args.n_goals
    navigation_type = args.navigation_type

    #set the seed
    random.seed(s)
    


    #target_pose = [args.GX,args.GY,args.GTH]

    rospy.init_node('obstacle_nav', disable_signals=True)

    listener = tf.TransformListener()
    odom_sub = rospy.Subscriber(TOPIC_odom, Odometry, odom_cb)
    laser_sub = rospy.Subscriber(TOPIC_laser, LaserScan, laser_cb)
    cmd_vel_sub = rospy.Subscriber(TOPIC_cmd_vel, Twist, cmdvel_cb)
    desired_cmd_vel_sub = rospy.Subscriber(TOPIC_desired_cmd_vel, Twist, desired_cmdvel_cb)

    # labirinto
    target_poses = [[2,2,0], [90,2,0], [98,48,0], [2,48,0], [42,24,0], [59,22,0],
                   [70,10,0], [18,20,0], [12,47,0], [66,45,0], [14,7,0]]
    
    # detection circle centers for reaching [95,25]
    #dangerous_centers = [[14, 5], [37, 12], [69,10], [98, 23]]
    
    # detection circle centers for reaching [16,44]
    dangerous_centers = [[4, 10], [14, 23], [4,36], [8, 47]]



    radius = 1
    build_dangerous_squares(dangerous_centers, radius)
    
    # saman
    #target_poses = [[-2, 8, 0], [15,6,0], [14,-3,0], [5,5,0], [0,0,0]]
    
    count_goals = 0

    # load image if the log flag is True
    if doLog == True:
       map_image = load_map_image(map_image_path)
    

    while not quit_request:
              
        
        #if input sub-lists contain only nans, go for continual random navigation
        if math.isnan(goal_list[0][0]) and math.isnan(goal_list[1][0]):

            print("Continual navigation -- Random goals")
            target_pose = random.choice(target_poses)
            execute_navigation_type(target_pose, navigation_type)
            count_goals += 1
            if doLog == True:
                write2file(path)
                map_image = draw_path_image(map_image)
                empty_log_buffer()

            if count_goals == num_goals:
                print("Reached the number of goals specified by the user")
                quit_request = True

        else:
            #if the first sublist contains only nan, select a random starting point and the robot will pursue the specified goal
            t_pose = goal_list[1]
            if math.isnan(goal_list[0][0]):

                print("Random starting pose (reaching ...) -- Defined goal")
                s_pose = random.choice(target_poses)
                reach_starting_pose(s_pose)
                print("Starting adaptive navigation")
                execute_navigation_type(t_pose, navigation_type)
                quit_request = True

            else:
                print("Defined starting pose (reaching ...) -- Defined goal")
                s_pose = goal_list[0]
                reach_starting_pose(s_pose)
                print("Starting adaptive navigation")
                execute_navigation_type(t_pose, navigation_type)
                quit_request = True

            if doLog == True:
                write2file(path)
                map_image = draw_path_image(map_image)
                empty_log_buffer()

    amod = compute_amod(amod_container)         
    print("Final (AMOD): %s\n" %amod)
    
        
    tg = compute_tg()
    print("Time [s] needed to reach the goal (TG): %s\n" %tg)

    print("Number of times (simulation steps) the robot was in dangerous zones (DD): %s\n" %dd_counter)
    
    # show the executed paths
    if doLog == True:  
        plt.figure()
        plt.imshow(map_image)
        plt.show()


    laser_sub.unregister()
    odom_sub.unregister()
    cmd_vel_sub.unregister()
    desired_cmd_vel_sub.unregister()

    print('Quit')



